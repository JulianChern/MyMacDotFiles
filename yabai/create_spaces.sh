#!/bin/sh

# define the space creating function
setup_space() {
  local idx="$1"
  local name="$2"
  local space=
  echo "setup space $idx : $name"

  space=$(yabai -m query --spaces --space "$idx")
  if [ -z "$space" ]; then
    yabai -m space --create
  fi

  yabai -m space "$idx" --label "$name"
}

#>>>>>>>>>>>>>>>>>>>> setup spaces >>>>>>>>>>>>>>>>>>>>
SPACE_NAMES=(code web research office enjoy more seven eight nine ten)
# delete redundant spaces, keep only 6 spaces on display 1
# REDUNDANT_SPACE1_IDX=$(yabai -m query --spaces --display 1 | jq '[.[].index | select(. > 6)][0]')
REDUNDANT_SPACE1_IDX=$(yabai -m query --spaces --display 1 | jq '[.[] |select(."is-native-fullscreen"!=true)]' | jq '[.[].index | select(. > 6)][0]')
while [ "$REDUNDANT_SPACE1_IDX" != "null" ];do
  osascript -e 'display notification "'"$REDUNDANT_SPACE1_IDX"'" with title "redunant_space1_idx" subtitle "destroying"'
  yabai -m space --focus $REDUNDANT_SPACE1_IDX
  yabai -m space --destroy
#   REDUNDANT_SPACE1_IDX=$(yabai -m query --spaces --display 1 | jq '[.[].index | select(. > 6)][0]')
  REDUNDANT_SPACE1_IDX=$(yabai -m query --spaces --display 1 | jq '[.[] |select(."is-native-fullscreen"!=true)]' | jq '[.[].index | select(. > 6)][0]')
done

# create the space for the desktop within the fullscreen app
EXTRA_FULLSCREEN_SPACE_INDICES=$(yabai -m query --spaces --display 1 | jq '[.[] |select(."is-native-fullscreen"==true)]' | jq '[.[].index | select(. > 6)]' | jq '.[]')
for SID in ${EXTRA_FULLSCREEN_SPACE_INDICES[@]}; do
  osascript -e 'display notification "'"$SID"'" with title "extra_fullscreen_space_idx" subtitle "creating"'
  setup_space $SID ${SPACE_NAMES[$SID-1]}
done


# create 6 spaces on display 1
for i in {1..6}; do
    setup_space $i ${SPACE_NAMES[$i-1]}
    yabai -m space --focus $i
    yabai -m space --display 1
done

# create more spaces on more displays
MORE_DISPLAY_INDICES=$(yabai -m query --displays |  jq '.[1:]' | jq '.[].index')
NEW_SID=7
for DID in ${MORE_DISPLAY_INDICES[@]}; do
    rtval=$(yabai -m query --spaces --space $NEW_SID | jq '."is-native-fullscreen"')
    if [ "$rtval" = "true" ];then
        NEW_SID=$(expr $NEW_SID + 1)
    else
        setup_space $NEW_SID ${SPACE_NAMES[$NEW_SID-1]}
        yabai -m space --focus $NEW_SID
        yabai -m space --display $DID
        NEW_SID=$(expr $NEW_SID + 1)

        setup_space $NEW_SID ${SPACE_NAMES[$NEW_SID-1]}
        yabai -m space --focus $NEW_SID
        yabai -m space --display $DID
        NEW_SID=$(expr $NEW_SID + 1)
    fi
done
#<<<<<<<<<<<<<<<<<<<< setup spaces <<<<<<<<<<<<<<<<<<<<

brew services restart sketchybar

